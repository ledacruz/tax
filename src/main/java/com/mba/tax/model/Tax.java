package com.mba.tax.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder

public class Tax {

    public String eventType;
    public String timestamp;
}
