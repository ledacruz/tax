package com.mba.tax.messaging;

import com.google.gson.Gson;
import com.mba.tax.model.Tax;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TaxListener {

    @Autowired
    TaxProducer taxProducer;

    @Value("${topic-choreography}")
    private String choreographyTopic;

    @Value("${topic-out}")
    private String orchestratorTopic;

    private final Logger logger = LoggerFactory.getLogger(TaxListener.class);

    private String producerMessage;

    @KafkaListener(topics = "${topic-choreography}", groupId = "group-tax")
    public void consumeChoreography(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Tax tax = convertStringToTax(message);

        switch (tax.getEventType()) {
            case "transferSender.sucess":
                producerMessage = "tax.sucess";
                //producerMessage = "tax.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            default:
                logger.info(String.format("$$ -> Consumed not a know Message -> %s",message));
        }
    }

    @KafkaListener(topics = "${topic-in}", groupId = "group-tax")
    public void consumeOrchestration(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Tax tax = convertStringToTax(message);

        switch (tax.getEventType()) {
            case "tax.requested":
                producerMessage = "tax.sucess";
                //producerMessage = "tax.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
        }
    }

    private void callProducer(String message, String topic){

        taxProducer.sendMessage(producerMessage, topic);
    }

    private Tax convertStringToTax(String message){
        Gson gson = new Gson();
        Tax tax = gson.fromJson(message, Tax.class);

        return tax;

    }
}
