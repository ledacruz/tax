package com.mba.tax.controller;

import com.mba.tax.messaging.TaxProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

    @RestController
    @RequestMapping(value = "/kafka")
    public class TaxController {
        private final TaxProducer taxProducer;

        @Autowired
        public TaxController(TaxProducer taxProducer) {
            this.taxProducer = taxProducer;
        }
        @PostMapping(value = "/publish")
        public void sendMessageToKafkaTopic(@RequestParam("message") String message){
            this.taxProducer.sendMessage(message, "");
        }
    }